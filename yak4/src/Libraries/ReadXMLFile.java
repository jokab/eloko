package Libraries;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.Node;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gargoylesoftware.htmlunit.javascript.host.dom.NodeList;

public class ReadXMLFile {


	  public static void getelement(int elementsId) {
		

	    try {
	    	
	    

		File fXmlFile = new File("/Users/206467959/workspace/yak4/resource/initialload.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);

		//optional, but recommended
		//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
		doc.getDocumentElement().normalize();

		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

		org.w3c.dom.NodeList nList = doc.getElementsByTagName("herd");

		System.out.println("----------------------------");

		for (int temp = 0; temp < nList.getLength(); temp++) {

			org.w3c.dom.Node nNode = nList.item(temp);

			System.out.println("\nCurrent Element :" + nNode.getNodeName());

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) nNode;

				System.out.println("day : " + eElement.getAttribute("id"));
				System.out.println("milkquantity : " + eElement.getElementsByTagName("milkquantity").item(0).getTextContent());
				System.out.println("skinsquanity : " + eElement.getElementsByTagName("skinsquanity").item(0).getTextContent());
				System.out.println("name1 : " + eElement.getElementsByTagName("name1").item(0).getTextContent());
				System.out.println("name2 : " + eElement.getElementsByTagName("name2").item(0).getTextContent());
				System.out.println("name3 : " + eElement.getElementsByTagName("name3").item(0).getTextContent());
				System.out.println("age1 : " + eElement.getElementsByTagName("age1").item(0).getTextContent());
				System.out.println("age2 : " + eElement.getElementsByTagName("age2").item(0).getTextContent());
				System.out.println("age3 : " + eElement.getElementsByTagName("age3").item(0).getTextContent());
				System.out.println("sex1 : " + eElement.getElementsByTagName("sex1").item(0).getTextContent());
				System.out.println("sex2 : " + eElement.getElementsByTagName("sex2").item(0).getTextContent());
				System.out.println("sex3 : " + eElement.getElementsByTagName("sex3").item(0).getTextContent());
				
				System.out.println("day : " + eElement.getAttribute("id"));
				String milk = ("milkquantity : " + eElement.getElementsByTagName("milkquantity").item(0).getTextContent());
				String skins = ("skinsquanity : " + eElement.getElementsByTagName("skinsquanity").item(0).getTextContent());
				String name1 = ("name1 : " + eElement.getElementsByTagName("name1").item(0).getTextContent());
				String name2 = ("name2 : " + eElement.getElementsByTagName("name2").item(0).getTextContent());
				String name3 = ("name3 : " + eElement.getElementsByTagName("name3").item(0).getTextContent());
				String age1 = ("age1 : " + eElement.getElementsByTagName("age1").item(0).getTextContent());
				String age2 = ("age2 : " + eElement.getElementsByTagName("age2").item(0).getTextContent());
				String age3 = ("age3 : " + eElement.getElementsByTagName("age3").item(0).getTextContent());
				String sex1 = ("sex1 : " + eElement.getElementsByTagName("sex1").item(0).getTextContent());
				String sex2 = ("sex2 : " + eElement.getElementsByTagName("sex2").item(0).getTextContent());
				String sex3 = ("sex3 : " + eElement.getElementsByTagName("sex3").item(0).getTextContent());
				
				
				text2(age1, age2, age3, milk, skins, name1, name2, name3, sex1, sex2, sex3);

			}
		}
		
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	    
	  }
	  
	  
	  
	  
	  public static String text2(  String age1, String age2, String age3,  String milk, String skins, 
			  String name1, String name2,  String name3, String sex1, String sex2, String sex3){
	       
	        
	        String defaultmilktext = " liters of milk";
	        String defaultskinstext = " skins of wool";
	        String defaultagetext = " years old";
	        
	
	         String finaltext = "In Stock:"+"\n"+milk+defaultmilktext+"\n"+skins+defaultskinstext+"\n"+"Herd:"+
	         name1+" "+age1+defaultagetext+"\n"+name2+" "+age2+defaultagetext+"\n"+name3+" "+age3+defaultagetext;
	         
	         return finaltext;
	    }
	  
	  public static void main(String argv[]) {

		  try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("company");
			doc.appendChild(rootElement);

			// staff elements
			Element staff = doc.createElement("Staff");
			rootElement.appendChild(staff);

			// set attribute to staff element
			Attr attr = doc.createAttribute("id");
			attr.setValue("1");
			staff.setAttributeNode(attr);

			// shorten way
			// staff.setAttribute("id", "1");

			// firstname elements
			Element firstname = doc.createElement("firstname");
			firstname.appendChild(doc.createTextNode("yong"));
			staff.appendChild(firstname);

			// lastname elements
			Element lastname = doc.createElement("lastname");
			lastname.appendChild(doc.createTextNode("mook kim"));
			staff.appendChild(lastname);

			// nickname elements
			Element nickname = doc.createElement("nickname");
			nickname.appendChild(doc.createTextNode("mkyong"));
			staff.appendChild(nickname);

			// salary elements
			Element salary = doc.createElement("salary");
			salary.appendChild(doc.createTextNode("100000"));
			staff.appendChild(salary);

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("C:\\file.xml"));

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);

			System.out.println("File saved!");

		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		}
	}

}
