package yakServices;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;


@Path("yak-shop/herd")
public class herdandstock {
    
    @GET
    @Produces("application/xml")
    public String myHerd() {
        
      return yakServices.herdxml();

    }
 
    @Path("{T}")
    @GET
    @Produces("application/xml")
    public String convertCtoFfromInput(@PathParam("T") Double T) {
        return yakServices.currentherdandstock(T);
 
      }

}
