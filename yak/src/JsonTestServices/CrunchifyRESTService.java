package JsonTestServices;

/**
 * @author Crunchify.com
 * 
 */

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/")
public class CrunchifyRESTService {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/quizcongo";

    static final String USER = "root";
    static final String PASS = "";

    public static Connection connectToDB(String dbUrl, String driver, String user, String pwd) {
        Connection conn = null;

        try {
            Class.forName(driver);
            System.out.println("Connecting to database...");

            conn = DriverManager.getConnection(dbUrl, user, pwd);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return conn;

    }

    public static JSONArray getAllTopics() {
        Connection conn = connectToDB(DB_URL, JDBC_DRIVER, USER, PASS);
        Statement stmt = null;
        ResultSet result = null;
        JSONArray topicArray = new JSONArray();

        try {
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM `topics`";
            result = stmt.executeQuery(sql);
            
            while (result.next()) {

                // String topicId = result.getString("topicId");
                String topicName = result.getString("topicName");
                String topicImage = result.getString("topicImageLink");
                String topicDescription = result.getString("topicDescription");

                JSONObject topic = createTopicObject(topicName, topicImage, topicDescription);
                topicArray.put(topic);

            }
            //topics.put("topics", topicArray);
            //System.out.println(topics.toString());

            result.close();
            stmt.close();
            conn.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return topicArray;

    }

    public static JSONObject getTopic(String Name) {
        Connection conn = connectToDB(DB_URL, JDBC_DRIVER, USER, PASS);
        Statement stmt = null;
        ResultSet result = null;
        JSONObject topic = new JSONObject();

        try {
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM `topics` WHERE `topicName`='" + Name + "'";
            result = stmt.executeQuery(sql);
            while (result.next()) {

                // String topicId = result.getString("topicId");
                String topicName = result.getString("topicName");
                String topicImage = result.getString("topicImageLink");
                String topicDescription = result.getString("topicDescription");

                topic = createTopicObject(topicName, topicImage, topicDescription);

            }
            System.out.println(topic.toString());

            result.close();
            stmt.close();
            conn.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return topic;

    }

    public static JSONObject createTopicObject(String name, String image, String description) {
        JSONObject topicObject = new JSONObject();
        try {
            // topicObject.put("topicId", id);
            topicObject.put("topicName", name);
            topicObject.put("topicImage", image);
            topicObject.put("topicDescription", description);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return topicObject;

    }

    // End Topic

    // Begin Questions
    public static JSONArray getquestion(String tName, int random, int limit) {
        Connection conn = connectToDB(DB_URL, JDBC_DRIVER, USER, PASS);
        Statement stmt = null;
        ResultSet result = null;
        JSONArray questionsArray = new JSONArray();
        JSONObject questionAnswerObj = new JSONObject();

        try {
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM `questions` WHERE `topicName` like'" + tName + "%'LIMIT "+limit;
            result = stmt.executeQuery(sql);
            while (result.next()) {

                String question = result.getString("question");
                String questionId =  result.getString("questionId");
                String[] answers ={"","",""};
                String answer="";
                
                for (int i = 1; i < 4; i++) {
                    
                        answers[i-1] = result.getString("answer"+i);
                    
                }
                answer = answers[random];
                questionAnswerObj = createQuestionObject(questionId, question, answer);
                questionsArray.put(questionAnswerObj);

            }
            System.out.println(questionAnswerObj.toString());

            result.close();
            stmt.close();
            conn.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return questionsArray;

    }

    public static JSONObject createQuestionObject(String questionId, String question, String answer) {
        
        JSONObject questionObject = new JSONObject();
        JSONArray choicesArray = new JSONArray();
        try {
            questionObject.put("question", question);
            questionObject.put("questionId", questionId);
            //questionObject.put("answer", answer);
            String correctAns="";

            String[] ans = answer.split("==");
            for (int i = 0; i < ans.length; i++) {
                if (ans[i].contains("jkjk")) {
                    ans[i] = ans[i].replace("jkjk", "");
                    correctAns = ans[i];
                }
                choicesArray.put(ans[i]);
            }
            questionObject.put("Answers", choicesArray);
            questionObject.put("correctAnswer", correctAns);
            questionObject.put("Random Number", generateRandomInteger(0, 2));
            

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return questionObject;

    }

    public static int generateRandomInteger(int min, int max) {
        SecureRandom rand = new SecureRandom();
        rand.setSeed(new Date().getTime());
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    @GET
    @Path("/topic/{tName}")
    @Produces("application/json")
    public Response verifyRESTService(@PathParam("tName") String topicName) {
        String result = "";
        if (topicName.equalsIgnoreCase("all")) {
            result = getAllTopics().toString();
        } else {
            result = getTopic(topicName).toString();
        }
        return Response.status(200).entity(result).build();
    }

/*    @GET
    @Path("/question/{tName}")
    @Produces("application/json")
    public Response getQuestionAnswer(@PathParam("tName") String topicName) {
        String result = "";
      
            int random = generateRandomInteger(0, 2);
            result = getquestion(topicName, random, 1).toString();
      
        return Response.status(200).entity(result).build();
    }*/
    
    @GET
    @Path("/question/{tName}/{num}")
    @Produces("application/json")
    public Response getQuestionsAnswers(@PathParam("tName") String topicName, @PathParam("num") int num) {
        String result = "";
      
            int random = generateRandomInteger(0, 2);
            result = getquestion(topicName, random, num).toString();
      
        return Response.status(200).entity(result).build();
    }

}
